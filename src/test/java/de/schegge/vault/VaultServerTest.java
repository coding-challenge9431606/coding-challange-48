package de.schegge.vault;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpRequest.BodyPublisher;
import java.net.http.HttpRequest.BodyPublishers;
import java.net.http.HttpResponse;
import java.util.Map;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

class VaultServerTest {
    private static VaultServer vaultServer;
    private static HttpRequest.Builder tokenizeBuilder;
    private static HttpRequest.Builder detokenizeBuilder;

    @BeforeAll
    static void setUp() throws IOException, URISyntaxException {
        vaultServer = new VaultServer();
        vaultServer.start();

        HttpRequest.Builder builder = HttpRequest.newBuilder().headers("Content-Type", "application(json;charset=UTF-8");
        tokenizeBuilder = builder.copy().uri(new URI("http://localhost:8080/tokenize"));
        detokenizeBuilder = builder.copy().uri(new URI("http://localhost:8080/detokenize"));
    }

    @AfterAll
    static void tearDown() {
        vaultServer.stop();
    }

    @Test
    void addToken() throws IOException, InterruptedException {
        Payload payload = new Payload();
        payload.setId("req-12345");
        payload.setData(Map.of("field1", "value1", "field2", "value2", "fieldn", "valuen"));
        HttpRequest request = tokenizeBuilder.copy().POST(of(payload)).build();
        try (HttpClient httpClient = HttpClient.newHttpClient()) {
            HttpResponse<Payload> response = httpClient.send(request, new JsonBodyHandler<>(Payload.class));
            assertEquals(201, response.statusCode());
            assertEquals("req-12345", response.body().getId());
            assertEquals(Set.of("field1", "field2", "fieldn"), response.body().getData().keySet());
            assertTrue(response.body().getData().values().stream().allMatch(s -> s.length() == 6));
        }
    }

    BodyPublisher of(Payload payload) throws JsonProcessingException {
        return BodyPublishers.ofString(new ObjectMapper().writeValueAsString(payload));
    }

    @Test
    void detokenize() throws IOException, InterruptedException {
        Payload payload = new Payload();
        payload.setId("req-12345");
        payload.setData(Map.of("field1", "value1", "field2", "value2", "fieldn", "valuen"));
        try (HttpClient httpClient = HttpClient.newHttpClient()) {
            HttpRequest request1 = tokenizeBuilder.copy().POST(of(payload)).build();
            HttpResponse<Payload> response = httpClient.send(request1, new JsonBodyHandler<>(Payload.class));
            assertEquals(201, response.statusCode());
            Payload payloadResponse = response.body();
            assertEquals("req-12345", payloadResponse.getId());
            assertEquals(Set.of("field1", "field2", "fieldn"), payloadResponse.getData().keySet());
            assertTrue(payload.getData().values().stream().allMatch(s -> s.length() == 6));

            HttpRequest request2 = detokenizeBuilder.copy().POST(of(payloadResponse)).build();
            HttpResponse<DetokenResponse> response2 = httpClient.send(request2, new JsonBodyHandler<>(DetokenResponse.class));
            assertEquals("req-12345", response2.body().getId());
            assertEquals(Set.of("field1", "field2", "fieldn"), response2.body().getData().keySet());
            assertTrue(response2.body().getData().values().stream().allMatch(s -> s.value().length() == 6));
        }
    }

    @Test
    void detokenizeUnknown() throws IOException, InterruptedException {
        Payload payload = new Payload();
        payload.setId("req-12345");
        payload.setData(Map.of("field1", "value1", "field2", "value2", "fieldn", "valuen"));
        try (HttpClient httpClient = HttpClient.newHttpClient()) {
            HttpRequest request1 = tokenizeBuilder.copy().POST(of(payload)).build();
            HttpResponse<Payload> response = httpClient.send(request1, new JsonBodyHandler<>(Payload.class));
            assertEquals(201, response.statusCode());
            Payload payloadResponse = response.body();
            assertEquals("req-12345", payloadResponse.getId());
            assertEquals(Set.of("field1", "field2", "fieldn"), payloadResponse.getData().keySet());
            assertTrue(payload.getData().values().stream().allMatch(s -> s.length() == 6));

            Payload payload2 = new Payload();
            payload2.setId("req-12346");
            HttpRequest request2 = detokenizeBuilder.copy().POST(of(payload2)).build();
            HttpResponse<DetokenResponse> response2 = httpClient.send(request2, new JsonBodyHandler<>(DetokenResponse.class));
            assertEquals(404, response2.statusCode());
            assertNull(response2.body());
        }
    }
}