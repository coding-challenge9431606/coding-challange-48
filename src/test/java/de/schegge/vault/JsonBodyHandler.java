package de.schegge.vault;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.net.http.HttpResponse;

public class JsonBodyHandler<W> implements HttpResponse.BodyHandler<W> {

    public static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    private final Class<W> type;

    public JsonBodyHandler(Class<W> type) {
        this.type = type;
    }

    private W toType(InputStream inputStream) {
        try (InputStream stream = inputStream) {
            return OBJECT_MAPPER.readValue(stream, type);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    @Override
    public HttpResponse.BodySubscriber<W> apply(HttpResponse.ResponseInfo responseInfo) {
        if (responseInfo.statusCode() < 200 || responseInfo.statusCode() > 299) {
            return HttpResponse.BodySubscribers.replacing(null);
        }
        return HttpResponse.BodySubscribers.mapping(HttpResponse.BodySubscribers.ofInputStream(), this::toType);
    }
}
