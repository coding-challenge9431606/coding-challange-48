package de.schegge.vault;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;

class DetokenizeHandler implements HttpHandler {

    private final ObjectMapper objectMapper = new ObjectMapper();

    private final Map<String, Map<String, Token>> vault;

    public DetokenizeHandler(Map<String, Map<String, Token>> vault) {
        this.vault = vault;
    }

    @Override
    public void handle(HttpExchange t) throws IOException {
        try {
            Payload payload = objectMapper.readValue(t.getRequestBody(), Payload.class);
            Map<String, Token> persisted = Optional.ofNullable(vault.get(payload.getId())).orElseThrow();
            Map<String, Detoken> detokenize = Tokenizer.detokenize(persisted, payload.getData());
            DetokenResponse resultPayload = new DetokenResponse(payload.getId(), detokenize);
            String response = objectMapper.writeValueAsString(resultPayload);
            t.sendResponseHeaders(200, response.length());
            try (OutputStream os = t.getResponseBody()) {
                os.write(response.getBytes());
            }
        } catch (IOException e) {
            t.sendResponseHeaders(400, 0);
        } catch (NoSuchElementException e) {
            t.sendResponseHeaders(404, 0);
        } catch (RuntimeException e) {
            t.sendResponseHeaders(500, 0);
        } finally {
            t.close();
        }
    }
}