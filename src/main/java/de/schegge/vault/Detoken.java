package de.schegge.vault;

record Detoken(boolean found, String value) {
}
