package de.schegge.vault;

import java.io.IOException;

public class Application {

    public static void main(String[] args) {
        try {
            new VaultServer().start();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
