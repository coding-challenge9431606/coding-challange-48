package de.schegge.vault;

import java.util.Map;

class DetokenResponse {
    public DetokenResponse() {
    }

    DetokenResponse(String id, Map<String, Detoken> data) {
        this.id = id;
        this.data = data;
    }

    private String id;
    private Map<String, Detoken> data;


    public String getId() {
        return id;
    }

    public Map<String, Detoken> getData() {
        return data;
    }
}
