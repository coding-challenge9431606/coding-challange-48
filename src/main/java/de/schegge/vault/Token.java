package de.schegge.vault;

public record Token(String key, String value) {
}
