package de.schegge.vault;

import java.security.SecureRandom;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toMap;

class Tokenizer {
    private static final SecureRandom SECURE_RANDOM = new SecureRandom();

    public static Map<String, Token> tokenize(Map<String, String> data) {
        return data.entrySet().stream().collect(toMap(Entry::getKey, Tokenizer::getToken));
    }

    private static Token getToken(Entry<String, String> e) {
        return new Token(createAlphanumericToken(e.getValue().length()), e.getValue());
    }

    private static String createAlphanumericToken(int length) {
        return SECURE_RANDOM.ints(48, 123)
                .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
                .mapToObj(i -> String.valueOf((char) i))
                .limit(length).collect(joining());
    }

    public static Map<String, Detoken> detokenize(Map<String, Token> tokens, Map<String, String> data) {
        if (data == null) {
            return Map.of();
        }
        record Valid(boolean valid, String key, String value) {
        }

        Set<String> fields = data.keySet();
        return tokens.entrySet().stream()
                .filter(e -> fields.contains(e.getKey()))
                .map(e -> new Valid(e.getValue().key().equals(data.get(e.getKey())), e.getKey(), e.getValue().value()))
                .collect(toMap(Valid::key, e -> new Detoken(e.valid(), e.value())));
    }
}
