package de.schegge.vault;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

class TokenizeHandler implements HttpHandler {

    private final ObjectMapper objectMapper = new ObjectMapper();

    private final Map<String, Map<String, Token>> vault;

    public TokenizeHandler(Map<String, Map<String, Token>> vault) {
        this.vault = vault;
    }

    @Override
    public void handle(HttpExchange t) throws IOException {
        try {
            Payload payload = objectMapper.readValue(t.getRequestBody(), Payload.class);
            Map<String, Token> tokenized = Tokenizer.tokenize(payload.getData());
            vault.put(payload.getId(), tokenized);
            Payload responsePayload = new Payload();
            responsePayload.setId(payload.getId());
            responsePayload.setData(tokenized.entrySet().stream().collect(Collectors.toMap(Entry::getKey, e->e.getValue().key())));
            String response = objectMapper.writeValueAsString(responsePayload);
            t.sendResponseHeaders(201, response.length());
            try (OutputStream os = t.getResponseBody()) {
                os.write(response.getBytes());
            }
        } catch (IOException e) {
            t.sendResponseHeaders(400, 0);
        } finally {
            t.close();
        }
    }
}