package de.schegge.vault;

import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;

class VaultServer {
    private final HttpServer httpServer;

    public VaultServer() throws IOException {
        Map<String, Map<String, Token>> vault = new ConcurrentHashMap<>();

        httpServer = HttpServer.create(new InetSocketAddress(8080), 0);
        httpServer.createContext("/tokenize", new TokenizeHandler(vault));
        httpServer.createContext("/detokenize", new DetokenizeHandler(vault));
        httpServer.setExecutor(Executors.newVirtualThreadPerTaskExecutor());
    }

    public void start() throws IOException {
        httpServer.start();
    }

    public void stop() {
        httpServer.stop(0);
    }
}
